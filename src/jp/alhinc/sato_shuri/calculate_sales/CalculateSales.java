package jp.alhinc.sato_shuri.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class CalculateSales {
	public static void main(String[] args) {

		BufferedReader br = null;

		//支店定義ファイルの中身(支店コード、支店名)を格納するmapを作成
		Map<String, String> branchMap = new HashMap<>();

		//売上ファイルの中身(支店コード、売上金額)を格納するmapを作成
		Map<String, Long> saleMap = new HashMap<>();

		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}


		try {

			File file = new File(args[0],"branch.lst");

			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String str;
			String[] branchInfo;

			while((str = br.readLine()) != null){

				branchInfo = str.split(",");

				if ((branchInfo.length != 2)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				String code = branchInfo[0];

				if (!isCode(code)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				String branch = branchInfo[1];

				branchMap.put(code, branch);
				saleMap.put(code, (long) 0);

			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if (br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		List <File> rcdAll = new ArrayList<File>();

		//Fileクラスのオブジェクトを生成する
		File dir = new File(args[0]);

		//listFilesメソッドを使用して一覧を取得する
		File[] list = dir.listFiles();
		BufferedReader rcf = null;

		for (int i = 0; i < list.length; i ++) {

			if(list[i].getName().matches("[0-9]{8}.rcd") && list[i].isFile()) {

				rcdAll.add(list[i]);
			}
		}

		String saleFile;
		String rcdFileNumber;
		List <Integer> num = new ArrayList<Integer>();

		try {

			for (int j=0; j < rcdAll.size(); j++) {

				saleFile = rcdAll.get(j).getName();
				rcdFileNumber = saleFile.substring(0, 8);

				int rcdFileNumbers = Integer.parseInt(rcdFileNumber);
				num.add(rcdFileNumbers);

				if (num.get(j) - j != num.get(0)) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			for(int i=0; i < rcdAll.size(); i++){

				File rcd = new File(args[0],rcdAll.get(i).getName());

				FileReader rf = new FileReader(rcd);
				rcf = new BufferedReader(rf);

				List <String> rcdList = new ArrayList<String>();
				String rcdFileRead;

				while((rcdFileRead = rcf.readLine()) != null) {
					rcdList.add(rcdFileRead);
				}

				if (rcdList.size() != 2) {
					System.out.println(rcd.getName() + "のフォーマットが不正です");
					return;
				}

				String saleCode = rcdList.get(0);
				String sl = rcdList.get(1);

				if (!(sl.matches("^\\-?[0-9]*\\.?[0-9]+$"))) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				Set<String> branchCode = branchMap.keySet();
				if (!branchCode.contains(saleCode)) {
					System.out.println(rcd.getName() + "の支店コードが不正です");
					return;
				}

				Long sale = Long.parseLong(sl);

				Long saleValue = (saleMap.get(saleCode));
				Long sales = saleValue + sale;

				String saleSize = String.valueOf(sales);

				if (saleSize.length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				saleMap.put(saleCode, sales);
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if (rcf != null) {
				try {
					rcf.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		File file = new File(args[0],"branch.out");
		if (!outFile(file, saleMap, branchMap)) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}

	public static boolean isCode(String code) {
		try {
			Integer.parseInt(code);

			if (code.matches("[0-9]{3}")) {
				return true;
			} else {
				return false;
			}
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public static boolean outFile (File file, Map<String, Long> map1, Map<String, String> map2) {

		BufferedWriter bw = null;

		try {

			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			String code;
			String name;
			Long sale;

			for(Entry<String, Long> set : map1.entrySet()) {

				code = set.getKey();
				sale = set.getValue();
				name = map2.get(code);

				bw.write(code + "," + name + "," + sale);
				bw.newLine();
			}
			return true;
		}catch(IOException e){
			return false;
		}finally {
			if (bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					return false;
				}
			}
		}
	}
}
